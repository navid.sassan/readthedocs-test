# Here are some notes from Navid

## Case with 11 3.5" HDD slots
Fractal R5 with extra 3-bay cage

## REISUB, better hard shutdown, recovering from an unresponsive system
[ArchWiki: Kernel](https://wiki.archlinux.org/index.php/Keyboard_shortcuts#Kernel)


## Apache httpd rewrite rule debug
`LogLevel debug rewrite:trace3`


## Python ternary
```python
condition_if_true if condition else condition_if_false

is_nice = True
state = "nice" if is_nice else "not nice"


>>> True or "Some"
True
>>>
>>> False or "Some"
'Some'
```



